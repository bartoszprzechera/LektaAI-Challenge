from DataCleaning import DataCleaner
from DataGathering import TargetWebsite, DataGatherer
from DataPreprocessing import DataPreprocessor

sites_to_crawl_computer = [
    TargetWebsite(
        url='https://www.apple.com/',
        how_deep=1,
        contains='www.apple.com',
        excluded=['order', 'account'],
        main_site_address='https://www.apple.com/')
]
sites_to_crawl_fruit = [
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Apple',
        contains='en.wikipedia.org',
        how_deep=0,
        main_site_address='https://en.wikipedia.org/wiki/Apple'),
    TargetWebsite(
        url='https://www.medicalnewstoday.com/articles/267290.php',
        how_deep=1,
        contains='medicalnewstoday.com',
        main_site_address='https://www.medicalnewstoday.com/'),
    TargetWebsite(
        url='https://www.worldatlas.com/articles/the-world-s-most-common-types-of-apples.html',
        how_deep=1,
        contains='worldatlas.com',
        main_site_address='https://www.worldatlas.com/')
]
twitter_search_queries_ruit = ['apple fruit -computer -pc -keyboard -ios -iphone -macbook -mac -pro',
                               'hungry fresh apple -computer -pc -keyboard -ios -iphone -macbook -mac -pro',
                               'healthy apple -computer -pc -keyboard -ios -iphone -macbook -mac -pro']
twitter_search_queries_computer = ['apple ios -fruit',
                                   'macbook apple -fruit',
                                   'iphone apple -fruit']
sites_to_crawl = [sites_to_crawl_fruit, sites_to_crawl_computer]
twitter_search_queries = [twitter_search_queries_ruit, twitter_search_queries_computer]
classes = ['fruit', 'pc']
text_must_have = ['apple']

data_gatherer = DataGatherer(classes, sites_to_crawl, twitter_search_queries, text_must_have)
data_gatherer.gather_data(twitter_max=100)

exclusions_fruit = ['computer', ' pc ', 'iphone', 'mac', 'macbook', 'ios']
exclusions_computer = ['fruit']
exclusions = [exclusions_fruit, exclusions_computer]

synonyms = []
inplaced_synonyms = 'apple'
text_must_haves = [['apple'], ['apple']]

data_cleaner = DataCleaner(classes, exclusions, synonyms, inplaced_synonyms, text_must_haves)
data_cleaner.clean()

data_preprocessor = DataPreprocessor()
data_preprocessor.get_trained_model(classes)

data_preprocessor.predict("I lost my new iphone !!!", data_cleaner)
data_preprocessor.predict("I like to eat apples", data_cleaner)
