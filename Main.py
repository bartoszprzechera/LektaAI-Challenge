from DataCleaning import DataCleaner
from DataGathering import TargetWebsite, DataGatherer
from DataPreprocessing import DataPreprocessor

sites_to_crawl_animal = [
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Mouse',
        how_deep=0),
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Rat',
        how_deep=0),
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Chinchilla',
        how_deep=0),
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Murinae',
        how_deep=0),
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Rodent',
        how_deep=0),
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Jerboa',
        how_deep=0),
    TargetWebsite(
        url='http://www.petmousefanciers.com/',
        how_deep=4,
        contains='www.petmousefanciers.com',
        main_site_address='http://www.petmousefanciers.com',
        excluded=['http://www.petmousefanciers.com/abuse'],
        is_question_mark_allowed=False),
    TargetWebsite(
        url='http://www.allaboutmice.co.uk',
        how_deep=-1,
        contains='http://www.allaboutmice.co.uk',
        main_site_address='http://www.allaboutmice.co.uk'),
    TargetWebsite(
        url='http://themouseconnection.forumotion.com/',
        how_deep=-1,
        contains='themouseconnection.forumotion.com',
        main_site_address='http://themouseconnection.forumotion.com')
]
sites_to_crawl_computer = [
    TargetWebsite(
        url='https://en.wikipedia.org/wiki/Computer_mouse',
        how_deep=0),
    TargetWebsite(
        url='https://www.overclock.net/forum/375-mice',
        contains='/forum/375-mice',
        how_deep=4,
        excluded=['?styleid'],
        main_site_address='https://www.overclock.net',
        is_question_mark_allowed=False),
    TargetWebsite(
        url='https://forums.tomsguide.com/tags/mice/',
        how_deep=2,
        contains='forums.tomsguide.com',
        main_site_address='https://forums.tomsguide.com'),
    TargetWebsite(
        url='https://hardforum.com/forums/mice-and-keyboards.124/',
        how_deep=2,
        contains='hardforum.com',
        main_site_address='https://hardforum.com/'),
    TargetWebsite(
        url='https://www.walmart.com/browse/computer-keyboards-mice/computer-mouse-mouse-pads/3944_3951_132959_1008621_4842284',
        how_deep=2,
        contains='www.walmart.com',
        main_site_address='https://www.walmart.com'),
    TargetWebsite(
        url='https://www.noelleeming.co.nz/shop/computers-tablets/accessories/computer-mice',
        how_deep=-1,
        contains='/shop/computers-tablets/accessories/computer-mice',
        main_site_address='https://www.noelleeming.co.nz'),
    TargetWebsite(
        url='https://www.currys.co.uk/gbuk/computing-accessories/computer-accessories/mice-and-keyboards/mice/318_3058_30060_xx_xx/xx-criteria.html',
        how_deep=-1,
        contains='/gbuk/computing-accessories/computer-accessories/mice-and-keyboards/mice/',
        main_site_address='https://www.currys.co.uk')
]
twitter_search_queries_animal = [
    'animal mouse -computer -computers -pc -pcs -keyboard',
    'animal mice -computer -computers -pc -pcs -keyboard',
    'animal chinchilla -computer -computers -pc -pcs -keyboard',
    'animal rat -computer -computers -pc -pcs -keyboard',
    'animal rodent -computer -computers -pc -pcs -keyboard',
    'dead rats -computer -computers -pc -pcs -keyboard',
    'fieldmouse -computer -computers -pc -pcs -keyboard',
    'jerboa -computer -computers -pc -pcs -keyboard',
    'little mouse -computer -computers -pc -pcs -keyboard',
    'mouse pet -computer -computers -pc -pcs -keyboard',
    'murine animal -computer -computers -pc -pcs -keyboard',
    'mus musculus',
    'rat -computer -computers -pc -pcs -keyboard'
]
twitter_search_queries_computer = [
    'cable mouse -animal',
    'computer mice -animal',
    'computer mouse -animal',
    'device mouse -animal',
    'gaming mouse -animal',
    'gaming mice -animal',
    'hardware mouse -animal',
    'mouse controller -animal',
    'peripheral mouse -animal',
    '"plug and play" mouse -animal',
    'ps2 mouse -animal',
    'usb mouse -animal',
    'wireless mouse -animal'
]
sites_to_crawl = [sites_to_crawl_animal, sites_to_crawl_computer]
twitter_search_queries = [twitter_search_queries_animal, twitter_search_queries_computer]
classes = ['animal', 'computer']
text_must_have = ['mice', 'mouse', ' rat ', ' rats ', 'rodent']

data_gatherer = DataGatherer(classes, sites_to_crawl, twitter_search_queries, text_must_have)
# data_gatherer.gather_data()

exclusions_animal = ['computer', ' pc ', 'keyboard']
exclusions_computer = ['animal']
exclusions = [exclusions_animal, exclusions_computer]

synonyms = ['mice', 'mices', 'rat', 'rats', 'mouserat', 'mouserats', 'mousing', 'chinchilla', 'chinchillas', 'mousie', 'mousies', 'mus']
inplaced_synonyms = 'mouse'
text_must_haves = [['mouse'], ['mouse']]

data_cleaner = DataCleaner(classes, exclusions, synonyms, inplaced_synonyms, text_must_haves)
data_cleaner.clean()

data_preprocessor = DataPreprocessor()
data_preprocessor.get_trained_model(classes)

data_preprocessor.predict("My mouse is hungry", data_cleaner)
data_preprocessor.predict("This mouse is more accurate than the other controller, even pads !!!! <3 ", data_cleaner)
data_preprocessor.predict("Can you buy me plug-and-play mouse with three heavy buttons?", data_cleaner)
data_preprocessor.predict("A lot of dead rats lie on the floor. It's disgusting and dangerous !!! You need to worry about it", data_cleaner)
data_preprocessor.predict("Mouse is a hardware that you need to plug to some pc to use it.", data_cleaner)
data_preprocessor.predict("My boss will give me wireless gaming mouse and keyboard", data_cleaner)
data_preprocessor.predict("Mickey mouse and donald duck are very popular animated animal heroes from cartoons.", data_cleaner)
data_preprocessor.predict("Mouse sit in the cage without water", data_cleaner)
data_preprocessor.predict("Two mouse fight with each others", data_cleaner)
data_preprocessor.predict("A computer mouse is a pointing device that detects two-dimensional motion relative to a surface.", data_cleaner)
data_preprocessor.predict("A mouse is a small rodent having a pointed snout, small rounded ears and a body-length scaly tail.", data_cleaner)
data_preprocessor.predict("This plug-and-play mouse is simple to set up - you just plug it into your USB and get back to business", data_cleaner)
