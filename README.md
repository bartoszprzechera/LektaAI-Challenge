## Prerequisites

Python 3.+

pandas, tweepy, requests, BeautifulSoup, sklearn, nltk, numpy, bayesian-optimization

```
pip install numpy pandas requests sklearn nltk bs4 tweepy bayesian-optimization
```

## Chalange

### General Considerations

* As a general recommendation, don't consider this as an evaluation exercise. It has been prepared as an open challenge, so during a few days you can develop and prove your skills in analysing complex problems, proposing some solutions, evaluating them and improving them in an iterative process
* This programming challenge is a very-well known disambiguation problem, and of course, there are hundreds of strategies and implementations just ready on the web to be downloaded and directly adapted. Of course, the ability to look for references in the whole community is a critical skill for any good programmer, but, in this case, the goal is to understand YOUR skills to face a problem. The goal is not to obtain the best possible solution, but to understand your approach to the challenge, and of course, the evolution of the strategy as you move forward and learn more about the domain
* The documentation generated during the process, covering aspects like the methodology used, the algorithms, data structures, cross-validation techniques, and so on, is perhaps more important that the code itself.
* It is also important to follow good software engineering practices, like writing a readable and testable code and making your development process transparent by commiting your work frequently.
* If you use some 3rd party components or libraries, please specify the motivation for their use. Take into account that working in a business environment imposes strong constraints on the availability of third-party libraries: not only in terms of licensing, but also considering their maintainability and robustness

### Problem description

You are given a set of sentences containing the word "mouse". Your task is to write a web service that
identifies the context of a whole sentence: **computer device** or a an **animal**.

It should be a simple API with one endpoint, that allows user to provide a sentence and get the classification
result as a response. The specific details of API design are up to you.

#### Sample Input
```
A computer mouse is a pointing device that detects two-dimensional motion relative to a surface.
A mouse is a small rodent having a pointed snout, small rounded ears and a body-length scaly tail.
This plug-and-play mouse is simple to set up - you just plug it into your USB and get back to business
```
#### Sample Output
```
device
animal
device
```
### Design and implementation approach

You have complete freedom to design and implement a solution to the whole (or part of the) problem as
specified. Still, the following guidelines can help you during this process.

* It is critical you clearly specify the methodology you are going to use. You have to assume the role of the analysts, designer and programmer of the solution. Obviously, there are many details in the specification of the problem left unspecified. You are free to take decisions in such cases, but you have to describe and explain your choices in the documentation.
* Iterative approach. In programming, it is quite unusual to start with the definitive specification of a solution to any problem. So, don't worry if you have to change your mind during the process. But, what is critical: motivate and document your decisions.
* The final result of this project should be a working prototype, not a proof of concept. Even though the time is limited and your solution will be far from complete, questions related to efficiency and robustness are crucial. Think carefully about the algorithms and data structures you decide to use, and keep in mind what would be needed to develop this project further.

### Test & training data

One of the hardest aspects in data science field is gathering the right data. You are not provided with a
dataset for development, training and/or testing for this problem, so you will need to gather and/or generate
the data on your own. It's crucial that as part of your work you define the structure of datasets and
document the way in which you're collecting them. If you develop some code that is used for this purpose,
include it together with the solution.