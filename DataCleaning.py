import re
from os import listdir
from os.path import isfile, join

import pandas as pd


class DataCleaner:
    def __init__(self, classes: list, exclusions: list = None, synonyms: list = None, inplaced_synonyms: str = None, text_must_haves: list = None, text_min_length: int = 20):
        self.classes = classes
        self.exclusions = exclusions
        self.synonyms = synonyms
        self.inplaced_synonyms = inplaced_synonyms
        self.text_must_haves = text_must_haves
        self.text_min_length = text_min_length

    @staticmethod
    def read_data_from_directory(directory: str):
        files = [f for f in listdir(directory) if isfile(join(directory, f))]
        data = pd.DataFrame(columns=['text'])
        for file in files:
            if '.csv' in file:
                data_in_file = pd.read_csv(directory + file)
                data = pd.concat([data, data_in_file], ignore_index=True)
            else:
                data_in_file = open(directory + file, "r", encoding="utf8")
                text = data_in_file.readlines()
                ds = pd.DataFrame(data=text, columns=['text'])
                data = data.append(ds, ignore_index=True)
                data_in_file.close()
        return data

    @staticmethod
    def clean_text(text: str):
        if type(text) != str:
            return " "
        text = re.sub(r'@\w+', '', text)  # mentions
        text = re.sub(r'http.?://[^\s]+[\s]?', '', text)  # urls
        text = re.sub(r'[^a-zA-Z\s]', ' ', text)  # symbols and digits
        text = re.sub(r'\brt\b', '', text)  # retweets
        text = re.sub(r'\bgt\b', '', text)
        text = re.sub(r'\bnbsp\b', ' ', text)
        text = re.sub(r'\n', ' ', text)  # Enters
        text = re.sub(r'(\b[A-Za-z]\b)', '', text)  # one letter words
        text = re.sub(r'\s+', ' ', text)  # extra white spaces
        text = text.lstrip()
        text = text.rstrip()
        text = text.lower()
        return text

    @staticmethod
    def remove_excluded(text: str, excluded_words: list):
        for excluded_word in excluded_words:
            if excluded_word in text:
                return " "
        return text

    def remove_synonyms(self, text: str):
        for synonym in self.synonyms:
            text = re.sub(r'\b%s\b' % synonym, self.inplaced_synonyms, text)
        return text

    @staticmethod
    def check_if_text_contain_some_of_must_have_words(text: str, must_have: list):
        if must_have is None:
            return text
        for word in must_have:
            if word in text:
                return text
        return " "

    @staticmethod
    def remove_by_texts(text: str):
        if len(text.split()) < 1:
            return " "
        return text if not (text.split()[0] == 'by' and len(text.split()) < 10) else " "

    def check_text_length(self, text: str):
        return text if len(text) >= self.text_min_length else " "

    def clean(self, save: bool = True):
        for clas, class_excluded_words, text_must_have in zip(self.classes, self.exclusions, self.text_must_haves):
            data = self.read_data_from_directory(clas + '/')
            print(clas + '. Records before cleaning: ' + str(len(data)))
            data.text = data.text.apply(lambda elem: self.clean_text(elem))
            data.text = data.text.apply(lambda elem: self.remove_excluded(elem, class_excluded_words))
            data.text = data.text.apply(lambda elem: self.remove_synonyms(elem))
            data.text = data.text.apply(lambda elem: self.check_if_text_contain_some_of_must_have_words(elem, text_must_have))
            data.text = data.text.apply(lambda elem: self.remove_by_texts(elem))
            data.text = data.text.apply(lambda elem: self.check_text_length(elem))
            data.drop_duplicates(subset=['text'], keep='first', inplace=True)
            data = pd.DataFrame(data.text, columns=['text'])
            print(clas + '. Records after cleaning: ' + str(len(data)))
            if save:
                data.to_csv(clas + '.csv', index=False)
